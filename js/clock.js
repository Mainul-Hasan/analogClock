/* Created by HP on 6/26/2018.*/
/* Author: Mainul Hasan.*/


// Select the canvas element in html document
const canvas = document.getElementById("myCanvas");

// Set the canvas to screen width & height
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Create a Drawing Object
const c = canvas.getContext('2d');

//Functions
//********************************************//

// adjust the radius of the clock based on screen sizes
function resolveResponsive() {
    let factor;
    if (canvas.width <= 992 && canvas.width >= 768) {
        factor = 0.8;
    } else if (canvas.width < 768 && canvas.width >= 480) {
        factor = 0.7;
    } else if (canvas.width < 480) {
        factor = 0.5;
    } else {
        factor = 0.9;
    }
    return factor;
}

// function to draw the clock
function drawClock() {
    drawFace(c, radius);
    drawNumbers(c, radius);
    drawMinuteMarkers(c, radius);
    drawTime(c, radius);
}

// draw the clock background
function drawFace(c, radius) {
    // Draw the white circle
    c.beginPath();
    c.arc(0, 0, radius, 0, 2 * Math.PI, false);
    c.fillStyle = "white";
    c.fill();
    // Create the gradient
    /*
    Syntax: context.createRadialGradient(x0,y0,r0,x1,y1,r1); - creates a radial / circular gradient
        * x0 	The x-coordinate of the starting circle of the gradient
        * y0 	The y-coordinate of the starting circle of the gradient
        * r0 	The radius of the starting circle
        * x1 	The x-coordinate of the ending circle of the gradient
        * y1 	The y-coordinate of the ending circle of the gradient
        * r1 	The radius of the ending circle
    */
    let grad = c.createRadialGradient(0, 0, radius * 0.95, 0, 0, radius * 1.05);
    //The addColorStop() method specifies the color stops, and its position along the gradient (between 0 & 1)
    grad.addColorStop(0, '#333');
    grad.addColorStop(0.5, 'white');
    grad.addColorStop(1, '#333');
    c.strokeStyle = grad;
    c.lineWidth = radius * 0.1;
    c.stroke();
    // Draw the clock center
    c.beginPath();
    c.arc(0, 0, radius * 0.1, 0, 2 * Math.PI);
    c.fillStyle = '#333';
    c.fill();
}

// draw the clock numbers
function drawNumbers(c, radius) {
    // Set the font size (of the drawing object) to 15% of the radius
    c.font = radius * 0.15 + "px Arial";
    // Set the text alignment to the middle and the center of the print position
    c.textAlign = "center";
    c.textBaseline = "middle";
    let ang;
    for (let num = 1; num < 13; num++) {
        ang = num * Math.PI / 6; // (360 / 12) * (pi / 180) = pi / 6
        c.rotate(ang); // rotate canvas by 30 degree
        c.translate(0, -radius * 0.85); // set (0,0) position of canvas to (0, -radius * 0.85)
        c.rotate(-ang); // rotate number to vertical
        c.fillText(num.toString(), 0, 0); // draw the number
        c.rotate(ang); // undo previous rotate
        c.translate(0, radius * 0.85); // re-align canvas to (0,0) position
        c.rotate(-ang); // undo the first rotation
    }
}

// draw the minute markers on the clock face
function drawMinuteMarkers(c, radius) {
    let ang;
    for (let num = 1; num < 61; num++) {
        ang = num * Math.PI / 30; // (360 / 60) * (pi / 180) = pi / 30
        // dont draw the minute points at hour positions
        if (num % 5 !== 0) {
            c.rotate(ang); // rotate canvas by 6 degree
            c.translate(0, -radius * 0.85); // set (0,0) position of canvas to (0, -radius * 0.85)
            //c.rotate(-ang); // rotate number to vertical
            c.beginPath();
            c.lineWidth = 2;
            c.lineCap = "round";
            c.moveTo(0, 0);
            c.lineTo(0, 6);
            c.stroke();
            c.translate(0, radius * 0.85); // re-align canvas to (0,0) position
            c.rotate(-ang); // undo the first rotation
        }
    }
}

// Draw the current time
function drawTime(c, radius) {
    // Get the current time and store into variables
    let timeNow = new Date();
    let hour = timeNow.getHours();
    let minute = timeNow.getMinutes();
    let second = timeNow.getSeconds();

    // hours
    hour = hour % 12; // convert into 12hr period
    // get the position of the hour hand
    hour = (hour * Math.PI / 6) + (minute * Math.PI / (6 * 60)) + (second * Math.PI / (360 * 60));
    drawHand(c, hour, radius * 0.5, radius * 0.07);

    // minutes
    // get the position of the minute hand
    minute = (minute * Math.PI / 30) + (second * Math.PI / (30 * 60));
    drawHand(c, minute, radius * 0.8, radius * 0.05);

    // seconds
    // get the position of the second hand
    second = (second * Math.PI / 30);
    drawHand(c, second, radius * 0.9, radius * 0.02, "red");

    // draw the circular centre on top again (for visual purpose only)
    c.beginPath();
    c.arc(0, 0, radius * 0.1, 0, 2 * Math.PI);
    c.fillStyle = '#333';
    c.fill();

}

function drawHand(c, pos, length, width, color = "#333") {
    c.beginPath();
    c.lineWidth = width;
    c.lineCap = "round";
    c.moveTo(0, 0);
    c.rotate(pos);
    c.lineTo(0, -length);
    c.strokeStyle = color;
    c.stroke();
    c.rotate(-pos);
}

//********************************************//

// Clock properties

// set clock position to the centre of canvas
c.translate(canvas.width / 2, canvas.height / 2);

let factor = resolveResponsive();

// set clock radius
const radius = (canvas.height / 2) * factor;

// draw the circle
// The setInterval() method calls a function or evaluates an expression at specified intervals (in milliseconds).
setInterval(drawClock, 1000); // drawClock will be called for each 1000 milliseconds.
